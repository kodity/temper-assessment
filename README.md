# Temper PHP Assessment


### Installation

1.Clone repo

2.Change to directory

````
cd temper
````   

3.Install dependencies

````
composer install
````

4.Generate application key:

````
php artisan key:generate
````

5.Install Node modules
````
npm install
````

6.Build

````
npm run prod
````

7.Setup the server. It will run on the port 8000.
````
php artisan serve
````

You can access the solution at http://localhost:8000/

