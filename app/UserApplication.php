<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserApplication extends Model
{
    public static function getRetentionCurve()
    {
        $filename = storage_path('csv/export.csv');
        $csvFile = file($filename);
        $retentionData = [];
        $steps = [0 => 0, 20 => 0, 40 => 0, 50 => 0, 70 => 0, 90 => 0, 99 => 0, 100 => 0, "total_users" => 0];

        // Starting at 1 means we're skipping the labels line
        for ($i = 1; $i < count($csvFile); $i++) {
            $data = str_getcsv($csvFile[$i], ';');

            // Getting the first day of the week
            $week = date("Y-m-d", strtotime('sunday this week', strtotime($data[1])));

            if (!isset($retentionData[$week])) {
                $retentionData[$week] = $steps;
            }

            $retentionData[$week]["total_users"]++;

            // Setting up the array
            foreach ($retentionData[$week] as $key => $value) {
                // If the percentage of the current step is greater than the value we're analyzing, increase value
                if (is_int($key) && $data[2] >= $key) {
                    $retentionData[$week][$key]++;
                }
            }
        }

        // Mounting chart object
        $chartArray = array(
            'type' => 'spline',
            'title' => [
                'text' => 'Weekly Retention Curve - PHP Assessment',
            ],
            'legend' => [
                'layout' => 'vertical',
                'align' => 'right',
                'verticalAlign' => 'middle'
            ],
            'tooltip' => ['valueSuffix' => '%']
        );

        // Pre-defined X Axis Values
        $stepLabelsArray = array(
            '0',
            '20',
            '40',
            '50',
            '70',
            '90',
            '99',
            '100'
        );

        $chartArray ['xAxis'] = array(
            'categories' => $stepLabelsArray,
            'title' => array(
                'text' => 'Progress in the application process'
            ),
            'labels' => array(
                'format' => '{value}%'
            ),
        );

        $chartArray ['yAxis'] = array(
            'title' => array(
                'text' => 'User percentage in this step'
            ),
            'labels' => array(
                'format' => '{value}%'
            ),
            'min' => '0',
            'max' => '100'
        );

        foreach ($retentionData as $weekNumber => $weekObject) {
            $valuesArray = array();

            foreach ($weekObject as $percentage => $userCount) {
                if (is_int($percentage)) $valuesArray[] = round(100 * $userCount / $weekObject['total_users']);
            }

            $chartArray ["series"] [] = array(
                "name" => $weekNumber,
                "data" => $valuesArray
            );
        }

        return $chartArray;
    }
}
