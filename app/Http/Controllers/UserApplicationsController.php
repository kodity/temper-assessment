<?php

namespace App\Http\Controllers;

use App\UserApplication;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Faker\Generator;

class UserApplicationsController extends Controller
{
    public function index()
    {
        return response()->json(UserApplication::getRetentionCurve());
    }
}
